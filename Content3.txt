<!DOCTYPE html>
<!-- saved from url=(0068)file:///C:/Users/PRATEEK%20KUMAR%20UADHYA/Downloads/Task3%20(3).html -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Content</title>
</head>

<body bgcolor="#0000FF">
<table align="center" bgcolor="#00800" width="600" border="2">
<caption><h1><u>CONTENT</u></h1><h1></h1></caption>
<tbody>
<tr>
<td>
<ol>
<h3><li><u>Differential Calculus</u></li></h3>
<ul type="square">
<li>Representation of function</li>
<li>Limit of a function</li>
<li>Continuity</li>
<li>Derivatives</li>
<li>Maxima and Minima
<ol type="a">
<li>Absolute Maxima and Minima</li>
<li>Mean Value Theorem</li>
</ol></li>
</ul>
<h3><li><u>Functions of Several Variables</u></li></h3>
<ul type="square">
<li>PDE - Homogeneous Function</li>
<li>Euler's Theorem</li>
<li>Jacobial</li>
<li>Taylor's Series</li>
<li>Lagrange's Method</li>
</ul>
<h3><li><u>Integral Calculus</u></li></h3>
<ul type="square">
<li>
<ol type="a">
<li>Definite Integrals</li>
<li>Indefinite Integrals</li>
<li>Properties of definite Integrals</li>
</ol>
</li>
<li>Subtitution Rule</li>
<li>Techniques of Integration
<ol type="a">
<li>Integration by parts</li>
<li>Trignometric Integrals</li>
<li>Trignometric Substitution</li>
</ol></li>
<li>Improper Integrals</li>
</ul>
<h3><li><u>Multiple Integrals</u></li></h3>
<ul type="square">
<li>Double Integrals</li>
<li>DI in Cartesian Co-ordinates</li>
<li>DI in Polar Co-ordinats</li>
<li>Change of Order of Integration</li>
<li>Triple Integrals
<ol type="a">
<li>Volume of Solids</li>
</ol>
</li></ul>
</ol>
</td>
</tr>
</tbody>
</table>


</body></html>